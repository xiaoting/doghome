import { Component, Input, OnInit } from '@angular/core';
import { Dog } from '../dog.model';
import { DogService } from '../dog.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'

@Component({
  selector: 'app-dog-detail',
  templateUrl: './dog-detail.component.html',
  styleUrls: ['./dog-detail.component.css']
})

export class DogDetailComponent implements OnInit {
  dog?: Dog;

  constructor(
    // To fetch the incoming data
    private route: ActivatedRoute,
    // To get the information for each single dog
    private dogService: DogService,
    // To navigate back to the Homepage
    private location: Location
  ) {}

  // Write the function for the Return-To-Homepage button
  return_homepage(): void {
    this.location.back()
  }

  ngOnInit(): void {
    // Get the dog's code by the incoming data
    this.route.params.subscribe((data) => {
      const dogId = data['status_code'];
      console.log(dogId)
    
    // Extend the new (key-value) pairs for Dog object
    this.dogService.getSingleDog(dogId).subscribe(data => {
         switch (data['status_code']) {
          case 203 : { data['description'] = "The lion, scientifically known as Panthera leo, stands as one of the most majestic and iconic creatures in the animal kingdom. Renowned for its regal appearance and powerful presence, the lion holds a special place in human culture, symbolizing strength, courage, and leadership."; break;}
          case 204 : { data['description'] = "Dogs, faithful companions, embody loyalty and joy. With diverse breeds, they offer boundless affection and unwavering support. From playful antics to heartwarming cuddles, dogs enrich lives, providing solace and smiles. Their presence, a testament to the enduring bond between humans and these four-legged friends."; break;}
          case 205 : { data['description'] = "Dogs, faithful companions, embody loyalty and joy. With diverse breeds, they offer boundless affection and unwavering support. From playful antics to heartwarming cuddles, dogs enrich lives, providing solace and smiles. Their presence, a testament to the enduring bond between humans and these four-legged friends."; break;}
          case 206 : { data['description'] = "Dogs, faithful companions, embody loyalty and joy. With diverse breeds, they offer boundless affection and unwavering support. From playful antics to heartwarming cuddles, dogs enrich lives, providing solace and smiles. Their presence, a testament to the enduring bond between humans and these four-legged friends."; break;}
          case 207 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 208 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 218 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 226 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 400 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 303 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 102 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 103 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 200 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 201 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          case 202 : { data['description'] = "Cats, enigmatic and graceful, epitomize independence. Their sleek forms and mesmerizing eyes exude a mysterious allure. From playful antics to serene moments, cats navigate life on their terms. As enigmatic companions, they weave their enchanter's spell, bringing comfort and charm to homes worldwide."; break;}
          case 301 : { data['description'] = "Dogs, known as humanity's best friends, share an unbreakable bond with us. Their diverse breeds showcase a remarkable spectrum of sizes, shapes, and personalities. Dogs offer unwavering companionship, loyalty, and unconditional love. Whether serving as working partners, loyal protectors, or beloved pets, dogs enrich our lives with their boundless affection and endless devotion."; break;}
          default: { data['description']="random text"; break; } 
         }
          this.dog = data;
        });
      console.log(this.dog)
  })
}

}