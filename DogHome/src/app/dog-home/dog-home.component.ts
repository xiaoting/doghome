import { Component } from '@angular/core';
import { DogService } from '../dog.service';
import { Dog } from '../dog.model';

@Component({
  selector: 'app-dog-home',
  templateUrl: './dog-home.component.html',
  styleUrls: ['./dog-home.component.css']
})
export class DogHomeComponent {
  // Define a Dog-Array
  dogs: Dog[] = [];

  constructor(
    private dogService: DogService) {}

  ngOnInit(): void {
    // Fetch the data from the defined service DogService
    this.dogService.getDogData().subscribe((data: any) => {
      console.log(data)
      // Save every single Dog data in the defined Dog-Array
      for(let i=0; i<=15; i++){
        this.dogs.push(data[i])
      }
      console.log(this.dogs)
    });
  }

}

