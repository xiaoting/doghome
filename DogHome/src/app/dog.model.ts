export interface Dog {
    status_code: number;
    title: string;
    image: any;
    url: string;
    description: string;
    showDescription: boolean;
  }