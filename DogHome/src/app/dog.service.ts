import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { Dog } from './dog.model';

@Injectable({
  providedIn: 'root'
})
export class DogService {

  constructor(private http: HttpClient) {}

    getDogData(): Observable<any[]> {
        let response1 =  this.http.get('/api/203.json');
        let response2 = this.http.get('/api/204.json');
        let response3 = this.http.get('/api/205.json');
        let response4 = this.http.get('/api/206.json');
        let response5 = this.http.get('/api/207.json');
        let response6 = this.http.get('/api/208.json');
        let response7 = this.http.get('/api/218.json');
        let response8 = this.http.get('/api/226.json');
        let response9 = this.http.get('/api/400.json');
        let response10 = this.http.get('/api/303.json');
        let response11 = this.http.get('/api/102.json');
        let response12 = this.http.get('/api/103.json');
        let response13 = this.http.get('/api/200.json');
        let response14 = this.http.get('/api/201.json');
        let response15 = this.http.get('/api/202.json');
        let response16 = this.http.get('/api/301.json');
        return forkJoin([response1, response2, response3, response4, response5, response6, response7, response8, response9, response10, response11, response12, response13, response14, response15, response16]);
    }

    getSingleDog(id: number): Observable<any>{
      let response = this.http.get('/api/'+id+'.json');
      return response
    }
}